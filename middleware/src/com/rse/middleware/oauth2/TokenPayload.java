/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rse.middleware.oauth2;

import java.util.List;

/**
 *
 * @author Ichlasul Affan
 */
public interface TokenPayload {
    String getEmail();

    List<String> getAudiences();

    String getIssuer();
}
