# NON TECHNICAL

* Microservice berperan sebagai penyedia API,
nanti fronend akan nembak API ini untuk mendapatkan data.

* Directory structure:
  * framework: 
    * resource: menerima routing aplikasi, kemudian mengarahkan ke lapisan service terkait.
    * service: implementasi business logic, pemanggilan ke lapisan entity model terkait fitur ataupun model lain, kemudian melakukan pengolahan data yang diterima dari lapisan entity.
    * entity: implementasi fungsi yang sering digunakan untuk mengakses ke basis data, seperti save, update, delete, dan list.
    * framework: konfigurasi aplikasi seperti routing, utility, product config dan kebutuhan lainnya.
    * delta: implementasi delta module dari aplikasi, dipisah sesuai dengan lapisan masing-masing.
  * middleware: segala hal yg berkaitan dengan auth (facebook auth, google auth)

### Flow 
![Microservice Message Flow](docs/microservice-arch.png)

![ABS Microservices Framework](docs/NewFramework.png)

### Development Step
1. Buat model ABS untuk fitur yang ingin dibuat.
2. Buat lapisan entity dan objek filter.
    1. Fungsi standar pada satu lapisan entity adalah:
        - Save Model
        - Update Model
        - Delete Model
        - List Model
    2. Membuat objek filter sesuai dengan atribut yang ingin di filter.
    3. Membuat fungsi list yang lebih fleksibel jika dibutuhkan, menggunakan objek filter yang sebelumnya telah dibuat.
3. Buat lapisan service, implementasi business logic sesuai kebutuhan. Pada lapisan ini, bisa melakukan pemanggilan ke lapisan entity model tersebut ataupun model lain. Pada lapisan ini, pemanggilan lapisan service lain juga dapat dilakukan.
4. Buat lapisan resource, pada resource 1 endpoint akan direpresentasikan oleh 1 fungsi pada resource yang akan memanggil fungsi pada lapisan service.
5. Buat routing, routing dikategorikan perfitur kemudian buat fungsi untuk setiap case yang ada pada fitur/model tersebut.

# TECHNICAL
### Build product 

pergi ke directory "<root>/framework", execute "bash build.sh"

  * output: NamaProduk.jar, disini mengandung semua library yg dibutuhkan, termasuk middleware.jar

### Build middleware

pergi ke directory "<root>/middleware", execute "ant"

  * output "<root>/middleware/dist/middleware.jar"

### Run product
> java -jar <NamaProduk.jar> -p <port,any>
> untuk akses, pergi ke http://localhost:[port], trus akses lewat route nya (lihat file route)

# Other

AbsDbOrm.jar => dihasilkan dari AbsDbOrm.java (ada di absmvc)
  - untuk dapetinnya coba clone absmvc, build disitu, copy lib/AbsDbOrm.jar nya

