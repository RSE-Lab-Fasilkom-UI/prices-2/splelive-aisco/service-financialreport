delta DExpenseService;
uses MExpenseService;

adds import Expense, ExpenseImpl from MExpenseModel;
adds import ExpenseEntity, ExpenseEntityImpl from MExpenseEntity;
adds import Program from MProgramModel;
adds import ProgramEntity, ProgramEntityImpl from MProgramEntity;
adds import ChartOfAccount from MChartOfAccountModel;
adds import ChartOfAccountEntity,ChartOfAccountEntityImpl from MChartOfAccountEntity;
adds import ABSHttpRequest from ABS.Framework.Http;
adds import Utility, UtilityImpl from ABS.Framework.Utility;
adds import ExpenseFilter, ExpenseFilterImpl from MExpenseFilter;

adds interface ExpenseService {
    List<Expense> list(ABSHttpRequest request);
    Expense detail(ABSHttpRequest request);
    List<Expense> save(ABSHttpRequest request);
    List<Expense> edit(ABSHttpRequest request);
    Expense update(ABSHttpRequest request);
    List<Expense> delete(ABSHttpRequest request);
}

adds class ExpenseServiceImpl implements ExpenseService {
    List<Expense> list(ABSHttpRequest request) {
        ExpenseEntity entity = new local ExpenseEntityImpl();
        ExpenseFilter filter = new local ExpenseFilterImpl();
        Utility utility = new local UtilityImpl();

        Bool hasKeyId = request.hasKey("id");
        Bool hasKeyIdCoas = request.hasKey("idCoas");
        Bool hasKeyIdPrograms = request.hasKey("idPrograms");

        if (hasKeyId) {
            String idStr = request.getInput("id");
            Int id = utility.stringToInteger(idStr);
            filter.setId(id);
        }

        if (hasKeyIdCoas) {
            String idCoas = request.getInput("idCoas");
            filter.setIdCoas(idCoas);
        }

        if (hasKeyIdPrograms) {
            String idPrograms = request.getInput("idPrograms");
            filter.setIdPrograms(idPrograms);
        }

        return entity.findExpenses(filter);
    }

    Expense detail(ABSHttpRequest request) {
        ExpenseEntity entity = new local ExpenseEntityImpl();
        String id = request.getInput("id");

        return entity.findById(id);
    }

    List<Expense> save(ABSHttpRequest request) {
        ExpenseEntity entity = new local ExpenseEntityImpl();
        ProgramEntity programEntity = new local ProgramEntityImpl();
        ChartOfAccountEntity coaEntity = new local ChartOfAccountEntityImpl();

        Utility utility = new local UtilityImpl();

        String datestamp = request.getInput("datestamp");
        String description = request.getInput("description");
        String amountStr = request.getInput("amount");
        Int amount = utility.stringToInteger(amountStr);
        String idProgram = request.getInput("idProgram");
        Int idProgramInt = utility.stringToInteger(idProgram);
        String idCoa = request.getInput("idCoa");
        Int idCoaInt = utility.stringToInteger(idCoa);

        Program program = programEntity.findById(idProgram);
        ChartOfAccount coa = coaEntity.findById(idCoa);

        return entity.saveExpense(datestamp, description, amount, idProgramInt, idCoaInt);
    }

    List<Expense> edit(ABSHttpRequest request) {
        ExpenseEntity entity = new local ExpenseEntityImpl();
        String id = request.getInput("id");
        Expense expense = entity.findById(id);

        List<Expense> dataModel = Nil;
        return appendright(dataModel, expense);
    }

    Expense update(ABSHttpRequest request) {
        ExpenseEntity entity = new local ExpenseEntityImpl();
        ProgramEntity programEntity = new local ProgramEntityImpl();
        ChartOfAccountEntity coaEntity = new local ChartOfAccountEntityImpl();

        Utility utility = new local UtilityImpl();

        String id = request.getInput("id");
        String datestamp = request.getInput("datestamp");
        String description = request.getInput("description");
        String amountStr = request.getInput("amount");
        Int amount = utility.stringToInteger(amountStr);
        String idProgram = request.getInput("idProgram");
        Int idProgramInt = utility.stringToInteger(idProgram);
        String idCoa = request.getInput("idCoa");
        Int idCoaInt = utility.stringToInteger(idCoa);

        Program program = programEntity.findById(idProgram);
        ChartOfAccount coa = coaEntity.findById(idCoa);

        return entity.updateExpense(id, datestamp, description, amount, idProgramInt, idCoaInt);
    }

    List<Expense> delete(ABSHttpRequest request) {
        ExpenseEntity entity = new local ExpenseEntityImpl();
        String id = request.getInput("id");

        return entity.deleteExpense(id);
    }
}
